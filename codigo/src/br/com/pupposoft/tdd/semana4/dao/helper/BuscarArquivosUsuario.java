package br.com.pupposoft.tdd.semana4.dao.helper;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;

import br.com.pupposoft.tdd.semana4.entidades.Usuario;
import br.com.pupposoft.tdd.semana4.enums.TipoPontoEnum;

public class BuscarArquivosUsuario  implements FileVisitor<Path>{
	private Usuario usuario;
	private TipoPontoEnum tipoPonto;

	private List<Path> arquivos;
	
	public BuscarArquivosUsuario(Usuario usuario, TipoPontoEnum tipoPonto) {
		this.usuario = usuario;
		this.tipoPonto = tipoPonto;
		this.arquivos = new ArrayList<>();
	}

	@Override
	public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
		return FileVisitResult.CONTINUE;
	}

	@Override
	public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
		if(this.usuario != null && 
				file.getParent().getFileName().toString().equals(usuario.getId().toString())){
			this.arquivos.add(file);
		}

		if(this.tipoPonto != null && file.getFileName().toString().contains(tipoPonto.name())){
			this.arquivos.add(file);
		}
		
		
		return FileVisitResult.CONTINUE;
	}

	@Override
	public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
		throw exc;
	}

	@Override
	public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
		return FileVisitResult.CONTINUE;
	}

	public List<Path> getArquivos() {
		return arquivos;
	}
}