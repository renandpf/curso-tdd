package br.com.pupposoft.tdd.semana4.dao.impl;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import br.com.pupposoft.tdd.semana4.dao.IArmazenamentoDao;
import br.com.pupposoft.tdd.semana4.dao.helper.BuscarArquivosUsuario;
import br.com.pupposoft.tdd.semana4.dao.helper.IOHelper;
import br.com.pupposoft.tdd.semana4.entidades.Ponto;
import br.com.pupposoft.tdd.semana4.entidades.Usuario;
import br.com.pupposoft.tdd.semana4.enums.TipoPontoEnum;

public class Armazenamento implements IArmazenamentoDao{
	
	private Path baseDir;
	private Path dadosUsuarioDir;
	
	public Armazenamento(Path baseDir) {
		this.baseDir = baseDir;
		if(baseDir == null){
			this.baseDir = Paths.get(System.getProperty("user.home"),"placarApp");
		}
		this.dadosUsuarioDir = Paths.get(this.baseDir.toString(),"dadosUsuarios");
	}

	@Override
	public void registrarPonto(Ponto ponto) throws IOException {
		
		Usuario usuario = ponto.getUsuario();
		
		Path userFile = Paths.get(dadosUsuarioDir.toString(), 
				usuario.getId()+"", 
				ponto.getTipo().name()+".txt");
		
		IOHelper.criarAtualizarArquivo(userFile);
		
        String msg = usuario.getNome()+"\n"+ponto.getQuantidade();
        Files.write(userFile, msg.getBytes());
	}

	@Override
	public List<Ponto> pesquisarPontos(Usuario usuario, TipoPontoEnum tipoPonto) throws IOException {
		List<Ponto> pontos = new ArrayList<>();
		
		BuscarArquivosUsuario buscador = new BuscarArquivosUsuario(usuario, tipoPonto);
		Files.walkFileTree(dadosUsuarioDir, buscador);

		List<Path> arquivos = buscador.getArquivos();
		for (Path arquivo : arquivos) {
			setDadosPonto(pontos, arquivo);
		}
		
		return pontos;
	}

	private void setDadosPonto(List<Ponto> pontos, Path arquivo) throws IOException {
		Usuario usuario = new Usuario();
		Ponto ponto = new Ponto();
		ponto.setUsuario(usuario);
		ponto.setTipo(TipoPontoEnum.valueOf(arquivo.getFileName().toString().replaceAll(".txt", "")));
		
		lerArquivoDePonto(pontos, arquivo, usuario, ponto);
	}

	private void lerArquivoDePonto(List<Ponto> pontos, Path arquivo, Usuario usuario, Ponto ponto) throws IOException {
		try (Stream<String> linhas = Files.lines(arquivo)) {
			linhas.forEach(linha -> {
				if(usuario.getNome() == null){
					usuario.setNome(linha);
				}else{
					ponto.setQuantidade(Integer.parseInt(linha.toString()));
				}
			});
			
			pontos.add(ponto);
		}
	}
}
