package br.com.pupposoft.tdd.semana4.dao.helper;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class IOHelper {

	public static void criarAtualizarArquivo(Path userFile) throws IOException {
		Files.createDirectories(userFile.getParent());
		if(Files.exists(userFile)){
			Files.delete(userFile);
		}
		Files.createFile(userFile);
	}


}
