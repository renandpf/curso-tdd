package br.com.pupposoft.tdd.semana4.dao;

import java.io.IOException;
import java.util.List;

import br.com.pupposoft.tdd.semana4.entidades.Ponto;
import br.com.pupposoft.tdd.semana4.entidades.Usuario;
import br.com.pupposoft.tdd.semana4.enums.TipoPontoEnum;

public interface IArmazenamentoDao {

	void registrarPonto(Ponto ponto) throws IOException;

	List<Ponto> pesquisarPontos(Usuario usuario, TipoPontoEnum tipoPonto) throws IOException;

}
