package br.com.pupposoft.tdd.semana4.exceptions;

public class PlacarException extends RuntimeException{
	private static final long serialVersionUID = 2484952076752886363L;

	public PlacarException(String message) {
		super(message);
	}

	public PlacarException(String message, Throwable cause) {
		super(message, cause);
	}
}
