package br.com.pupposoft.tdd.semana4.service.impl;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import br.com.pupposoft.tdd.semana4.dao.IArmazenamentoDao;
import br.com.pupposoft.tdd.semana4.entidades.Ponto;
import br.com.pupposoft.tdd.semana4.entidades.Usuario;
import br.com.pupposoft.tdd.semana4.enums.TipoPontoEnum;
import br.com.pupposoft.tdd.semana4.exceptions.PlacarException;
import br.com.pupposoft.tdd.semana4.service.IPlacarService;

public class PlacarService implements IPlacarService{
	
	private IArmazenamentoDao armazanamentoDao;

	public PlacarService(IArmazenamentoDao armazanamento) {
		this.armazanamentoDao = armazanamento;
	}

	@Override
	public Ponto registrarPonto(Usuario usuario, TipoPontoEnum tipoPonto, Integer quantidade) {
		
		try {
			Ponto ponto = new Ponto();
			ponto.setUsuario(usuario);
			ponto.setTipo(tipoPonto);
			ponto.setQuantidade(quantidade);
			
			this.armazanamentoDao.registrarPonto(ponto);
			
			return ponto;
		} catch (IOException e) {
			throw new PlacarException("Ocorreu um erro ao registrar o ponto",e);
		}
	}

	@Override
	public List<Ponto> getPontosUsuario(Usuario usuario) {
		try {
			List<Ponto> pontos = this.armazanamentoDao.pesquisarPontos(usuario, null);
			return pontos;
		} catch (IOException e) {
			throw new PlacarException("Ocorreu um erro ao obter os pontos do usuário",e);
		}
		
	}

	@Override
	public List<Ponto> getRanking(TipoPontoEnum tipoPonto) {
		try {
			List<Ponto> pontos = this.armazanamentoDao.pesquisarPontos(null, tipoPonto);
			return pontos.stream().sorted(Collections.reverseOrder()).collect(Collectors.toList());
		} catch (IOException e) {
			throw new PlacarException("Ocorreu um erro ao obter o ranking do tipo de ponto",e);
		}
	}
	
}
