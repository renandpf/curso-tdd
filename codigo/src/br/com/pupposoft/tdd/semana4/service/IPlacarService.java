package br.com.pupposoft.tdd.semana4.service;

import java.util.List;

import br.com.pupposoft.tdd.semana4.entidades.Ponto;
import br.com.pupposoft.tdd.semana4.entidades.Usuario;
import br.com.pupposoft.tdd.semana4.enums.TipoPontoEnum;
import br.com.pupposoft.tdd.semana4.exceptions.PlacarException;

public interface IPlacarService {
	
	Ponto registrarPonto(Usuario usuario, TipoPontoEnum tipoPonto, Integer quantidade) throws PlacarException;
	List<Ponto> getPontosUsuario(Usuario usuario) throws PlacarException;
	List<Ponto> getRanking(TipoPontoEnum tipoPonto) throws PlacarException;
}
