package br.com.pupposoft.tdd.semana4.controller;

import java.util.List;
import java.util.stream.Collectors;

import br.com.pupposoft.tdd.semana4.entidades.Ponto;
import br.com.pupposoft.tdd.semana4.entidades.Usuario;
import br.com.pupposoft.tdd.semana4.enums.TipoPontoEnum;
import br.com.pupposoft.tdd.semana4.service.IPlacarService;

public class Placar {
	
	
	private IPlacarService placarService;
	
	
	
	public Placar(IPlacarService placarService) {
		this.placarService = placarService;
	}

	/**
	 * Registrar um tipo de ponto para um usuário. Por exemplo: o usuário "guerra" recebeu "10" pontos do tipo "estrela"
	 * @param nomeUsuario TODO
	 */
	public String registrarPonto(int idUsuario, String nomeUsuario, TipoPontoEnum tipoPonto, int quantidadePontos){
		Usuario usuario = new Usuario();
		usuario.setId(idUsuario);
		usuario.setNome(nomeUsuario);
		Ponto pontoRegistrado = this.placarService.registrarPonto(usuario, tipoPonto, quantidadePontos);
		
		String nomeUser = pontoRegistrado.getUsuario().getNome();
		
		return "o usuário "+nomeUser+" recebeu "+quantidadePontos+" pontos do tipo "+tipoPonto.name().toLowerCase()+"";
	}
	
	/**
	 * Retornar todos os pontos de um usuário. 
	 * Por exemplo: ao pedir os pontos do usuário "guerra" ele me retornaria que possui "20" pontos do tipo "moeda" e 
	 * "25" pontos do tipo "estrela". 
	 * Um tipo de ponto que o usuário não possuir, não deve ser retornado com valor "0". 
	 * Por exemplo: se o usuário "guerra" não possui pontos do tipo "energia", esses não devem ser incluídos na resposta.
	 */
	public List<String> getPontosUsuario(int idUsuario){
		
		Usuario usuario = new Usuario();
		usuario.setId(idUsuario);
		
		List<Ponto> pontos = this.placarService.getPontosUsuario(usuario);
		
		return pontos.stream().map(Ponto::getPontosusuarioInfo).collect(Collectors.toList());
	}

	/**
	 * Retornar ranking de um tipo de ponto, com a lista de usuário que possuem aquele ponto ordenados do que possui mais para 
	 * o que possui menos. 
	 * Por exemplo: ao pedir o ranking de "estrela", seria retornado "guerra" com "25", "fernandes" com "19" e "rodrigo" com "17". 
	 * Um usuário que não possui pontos daquele tipo não seria incluído no ranking. 
	 * Por exemplo, o usuário "toco" sem pontos do tipo "estrela" 
	 * não seria incluído.
	 * 
	 * @param tipoPonto
	 * @return
	 */
	public List<String> getRanking(TipoPontoEnum tipoPonto){
		List<Ponto> pontos =  this.placarService.getRanking(tipoPonto);
		return pontos.stream().map(Ponto::getRankingInfo).collect(Collectors.toList());
	}
	
}
