package br.com.pupposoft.tdd.semana4.entidades;

import br.com.pupposoft.tdd.semana4.enums.TipoPontoEnum;

public class Ponto implements Comparable<Ponto> {
	private Integer quantidade;
	private TipoPontoEnum tipo;
	private Usuario usuario;
	
	public Integer getQuantidade() {
		return quantidade;
	}
	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}
	public TipoPontoEnum getTipo() {
		return tipo;
	}
	public void setTipo(TipoPontoEnum tipo) {
		this.tipo = tipo;
	}
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public String getPontosusuarioInfo() {
		return this.quantidade+" pontos do tipo "+this.tipo.name().toLowerCase();
	}
	public String getRankingInfo() {
		return this.getUsuario().getNome()+" com "+this.quantidade;
	}
	@Override
	public int compareTo(Ponto outroPonto) {
		return Integer.compare(this.quantidade, outroPonto.getQuantidade());
	}
}
