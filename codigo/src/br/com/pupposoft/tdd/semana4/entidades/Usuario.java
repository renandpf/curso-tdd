package br.com.pupposoft.tdd.semana4.entidades;

import java.util.List;

public class Usuario {
	private Integer id;
	private String nome;
	private List<Ponto> pontos;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public List<Ponto> getPontos() {
		return pontos;
	}
	public void setPontos(List<Ponto> pontos) {
		this.pontos = pontos;
	}
}
