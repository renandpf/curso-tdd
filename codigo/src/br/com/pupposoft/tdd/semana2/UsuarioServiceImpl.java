package br.com.pupposoft.tdd.semana2;

import java.util.ArrayList;
import java.util.List;

import br.com.pupposoft.tdd.semana2.entidades.Usuario;
import br.com.pupposoft.tdd.semana2.exceptions.UsuarioComNomeVazioException;
import br.com.pupposoft.tdd.semana2.exceptions.UsuarioInexistenteException;
import br.com.pupposoft.tdd.semana2.exceptions.UsuarioJaRegistradoException;

public class UsuarioServiceImpl {
	private List<Usuario> _usuariosRegistrados = new ArrayList<>();

	public void registraUsuario(String nomeUsuario) throws UsuarioJaRegistradoException, UsuarioComNomeVazioException, UsuarioInexistenteException{
		if(nomeUsuario == null){
			throw new UsuarioInexistenteException("Não pode registrar usuários inexistente!");
		}			
		if(nomeUsuario.isEmpty()){
			throw new UsuarioComNomeVazioException("Não pode registrar usuario com nome vazio!");
		}

		Usuario novoUsuario = new Usuario(nomeUsuario);
		if(this._usuariosRegistrados.contains(novoUsuario)){
			throw new UsuarioJaRegistradoException("Já existe usuário com o nome "+nomeUsuario);
		}

		this._usuariosRegistrados.add(novoUsuario);
	}

	public List<Usuario> get_usuarios() {
		return new ArrayList<>(this._usuariosRegistrados);
	}

}
