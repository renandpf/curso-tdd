package br.com.pupposoft.tdd.semana2.exceptions;

public class UsuarioJaRegistradoException extends Exception {
	private static final long serialVersionUID = 2967436575783716155L;

	public UsuarioJaRegistradoException(String mensagem) {
		super(mensagem);
	}

}
