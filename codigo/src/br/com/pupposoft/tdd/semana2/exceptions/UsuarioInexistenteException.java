package br.com.pupposoft.tdd.semana2.exceptions;

public class UsuarioInexistenteException extends Exception {
	private static final long serialVersionUID = -4538153732435560890L;

	public UsuarioInexistenteException(String mensagem) {
		super(mensagem);
	}
}
