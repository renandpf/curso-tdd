package br.com.pupposoft.tdd.semana2.exceptions;

public class UsuarioComNomeVazioException extends Exception {
	private static final long serialVersionUID = 7098974292262967172L;

	public UsuarioComNomeVazioException(String mensagem) {
		super(mensagem);
	}

}
