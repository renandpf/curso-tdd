package br.com.pupposoft.tdd.semana3.services;

import br.com.pupposoft.tdd.semana3.entities.ContaCorrente;

public interface ServicoRemoto {
	ContaCorrente recuperarConta(String numeroConta);
	void persistirConta(ContaCorrente contaCorrente);
}
