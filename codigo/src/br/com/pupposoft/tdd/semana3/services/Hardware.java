package br.com.pupposoft.tdd.semana3.services;

import br.com.pupposoft.tdd.semana3.exceptions.HardwareException;

public interface Hardware {
	String pegarNumeroDaContaCartao() throws HardwareException;
	void entregarDinheiro()  throws HardwareException;
	void lerEnvelope()  throws HardwareException;
}
