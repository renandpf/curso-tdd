package br.com.pupposoft.tdd.semana3.helper;

public class GlobalMessage {
	public static final String LOGIN_SUCESSO = "Usuário Autenticado";
	public static final String LOGIN_ERRO = "Não foi possível autenticar o usuário";
	public static final String SALDO = "O saldo é R$";
	public static final String DEPOSITO_SUCESSO = "Depósito recebido com sucesso";
	public static final String CONTA_NAO_ENCONTRADA = "Não foi encontrada a conta corrente informada.";
	public static final String SAQUE_SUCESSO = "Retire seu dinheiro";
	public static final String ERRO_HARDWARE = "Houve uma falha de funcionamento do hardware.";
	public static final String SALDO_INSUFICIENTE = "Saldo insuficiente";

}
