package br.com.pupposoft.tdd.semana3.exceptions;

import br.com.pupposoft.tdd.semana3.helper.GlobalMessage;

public class HardwareException extends Exception {
	private static final long serialVersionUID = -6211227768468936967L;

	public HardwareException() {
		super(GlobalMessage.ERRO_HARDWARE);
	}
	
}
