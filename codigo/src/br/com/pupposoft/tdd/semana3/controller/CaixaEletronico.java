package br.com.pupposoft.tdd.semana3.controller;

import java.time.LocalDateTime;

import br.com.pupposoft.tdd.semana3.entities.ContaCorrente;
import br.com.pupposoft.tdd.semana3.entities.Movimento;
import br.com.pupposoft.tdd.semana3.exceptions.HardwareException;
import br.com.pupposoft.tdd.semana3.helper.GlobalMessage;
import br.com.pupposoft.tdd.semana3.helper.TipoMovimentoEnum;
import br.com.pupposoft.tdd.semana3.services.Hardware;
import br.com.pupposoft.tdd.semana3.services.ServicoRemoto;

public class CaixaEletronico {
	
	private Hardware hardware;
	
	private ServicoRemoto servicoRemoto;
	
	//considere que a conta corrente será mantida na sessão do usuário
	private ContaCorrente contaCorrente;
	
	public CaixaEletronico(Hardware hardware, ServicoRemoto servicoRemoto) {
		super();
		this.hardware = hardware;
		this.servicoRemoto = servicoRemoto;
	}

	/*
	 * Ao executar o método login(), e a execução for com sucesso, deve retornar a mensagem "Usuário Autenticado". 
	 * Caso falhe, deve retornar "Não foi possível autenticar o usuário"
	 */
	public String logar(String usuario, String senha){
		try {
			String numeroConta = this.hardware.pegarNumeroDaContaCartao();
			this.contaCorrente = this.servicoRemoto.recuperarConta(numeroConta);
			if(contaCorrente == null){
				return GlobalMessage.CONTA_NAO_ENCONTRADA;
			}

			if(this.autenticarUsuario(usuario, senha, contaCorrente)){
				return GlobalMessage.LOGIN_SUCESSO;
			}
			
		} catch (Exception e) {
		}
		
		return GlobalMessage.LOGIN_ERRO;
	}

	/*
	 * Ao executar o método sacar(), e a execução for com sucesso, deve retornar a mensagem "Retire seu dinheiro". 
	 * Se o valor sacado for maior que o saldo da conta, a classe CaixaEletronico deve retornar uma String dizendo 
	 * "Saldo insuficiente".
	 */
	public String sacar(double valorSaque) {
		String numeroConta = null;
		try {
			numeroConta = this.hardware.pegarNumeroDaContaCartao();
		} catch (HardwareException e) {
			return e.getMessage();
		}
		
		this.contaCorrente = this.servicoRemoto.recuperarConta(numeroConta);
		if(contaCorrente == null){
			return GlobalMessage.CONTA_NAO_ENCONTRADA;
		}

		double saldoAtual = this.getSaldo(this.contaCorrente);
		if(valorSaque > saldoAtual){
			return GlobalMessage.SALDO_INSUFICIENTE;
		}
		
		try {
			this.hardware.entregarDinheiro();
		} catch (HardwareException e) {
			return e.getMessage();
		}
		
		this.criarMovimento(valorSaque, "Saque", this.contaCorrente, TipoMovimentoEnum.DEBITO);
		
		this.servicoRemoto.persistirConta(this.contaCorrente);
		
		return GlobalMessage.SAQUE_SUCESSO;
	}
	
	/*
	 * Ao executar o método depositar(), e a execução for com sucesso, deve retornar a mensagem "Depósito recebido com sucesso"
	 * O método persistirConta() da interface ServicoRemoto deve ser chamado apenas no caso de ser feito algum saque ou depósito com sucesso.
	 */
	public String depositar(String numeroConta, double valorDeposito, String descricao){
		ContaCorrente contaCorrenteDestino = this.servicoRemoto.recuperarConta(numeroConta);
		if(contaCorrenteDestino == null){
			return GlobalMessage.CONTA_NAO_ENCONTRADA;
		}

		try {
			this.hardware.lerEnvelope();
		} catch (HardwareException e) {
			return e.getMessage();
		}
		
		this.criarMovimento(valorDeposito, descricao, contaCorrenteDestino, TipoMovimentoEnum.CREDITO);
		
		this.servicoRemoto.persistirConta(contaCorrenteDestino);
		return GlobalMessage.DEPOSITO_SUCESSO;
	}

	//Ao executar o método saldo(), a mensagem retornada deve ser "O saldo é R$xx,xx" com o valor do saldo.
	public String saldo(){
		double saldo = getSaldo(this.contaCorrente);
		return GlobalMessage.SALDO+saldo;
	}

	
	//Os métodos abaixo poderiam ficar numa classe "Service", para centralizar as regras de negócio na camada de serviço
	//Por falta de tempo não foi implementado dessa forma
	private void criarMovimento(double valor, String descricao, ContaCorrente contaCorrente, TipoMovimentoEnum tipoMovimento) {
		valor = Math.abs(valor);
		if(tipoMovimento == TipoMovimentoEnum.DEBITO){
			valor *= -1; 
		}
		
		Movimento deposito = new Movimento();
		deposito.setContaCorrente(contaCorrente);
		deposito.setDataHora(LocalDateTime.now());
		deposito.setDescricaoResumida(descricao);
		deposito.setValor(valor);
		
		contaCorrente.getMovimentos().add(deposito);
	}
	
	private double getSaldo(ContaCorrente contaCorrente) {
		double saldo = contaCorrente.getMovimentos().stream().mapToDouble(Movimento::getValor).sum();
		return saldo;
	}
	
	private boolean autenticarUsuario(String usuario, String senha, ContaCorrente contaCorrente) {
		String usuarioRetornado = contaCorrente.getCorrentista().getUsuario();
		String senhaRetornada = contaCorrente.getCorrentista().getSenha();
		
		return usuarioRetornado.equals(usuario) && 
				senhaRetornada.equals(senha);
	}
}
