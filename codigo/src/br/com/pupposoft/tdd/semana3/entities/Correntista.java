package br.com.pupposoft.tdd.semana3.entities;

import java.util.List;

public class Correntista {
	private String usuario;
	private String senha;
	
	private List<ContaCorrente> contasCorrentes;

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public List<ContaCorrente> getContasCorrentes() {
		return contasCorrentes;
	}

	public void setContasCorrentes(List<ContaCorrente> contasCorrentes) {
		this.contasCorrentes = contasCorrentes;
	}
}
