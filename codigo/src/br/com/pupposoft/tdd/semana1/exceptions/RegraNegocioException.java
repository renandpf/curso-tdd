package br.com.pupposoft.tdd.semana1.exceptions;

public class RegraNegocioException extends RuntimeException {
	private static final long serialVersionUID = 7715059031682206301L;

	public RegraNegocioException(String message) {
		super(message);
	}
	
}
