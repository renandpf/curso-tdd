package br.com.pupposoft.tdd.semana1;

import java.util.ArrayList;
import java.util.List;

import br.com.pupposoft.tdd.semana1.exceptions.RegraNegocioException;

public class CamelCaseImpl {

	//TODO - Adicionar todos os possíveis
	private final String[] caracteresInvalidos = {"!","@","#","$","%","*","(",")"};


	private void validaEntrada(String camelCase){
		for (int i = 0; i <= 9; i++) {
			if(camelCase.startsWith(i+"")){
				throw new RegraNegocioException("Não deve começar com números");
			}
		}
		for (String caracterInvalido : caracteresInvalidos) {
			if(camelCase.contains(caracterInvalido)){
				throw new RegraNegocioException("Caracteres especiais não são permitidos, somente letras e números");
			}
		}
	}

	public List<String> toList(String camelCase) {
		this.validaEntrada(camelCase);
		List<String> lista = new ArrayList<>();

		this.processarCamelCaseArray(camelCase, lista);

		lista = this.aplicarAjusteLista(lista);
		return lista;
	}

	private void processarCamelCaseArray(String camelCase, List<String> lista){
		StringBuilder sb = new StringBuilder();
		char cameCaseArray[] = camelCase.toCharArray();
		for (int i = 0; i < cameCaseArray.length; i++) {
			if (this.isMaiuscula(cameCaseArray[i]+"") && i > 0){
				lista.add(sb.toString());
				sb = new StringBuilder();
				sb.append(cameCaseArray[i]);
			}else{sb.append(cameCaseArray[i]);}
		}
		lista.add(sb.toString());
	}

	//TODO - deixar com 10 linhas - não tive mais tempo... :(( 
	private List<String> aplicarAjusteLista(List<String> listaNaoAjustada){
		List<String> listaAjustada = new ArrayList<>();
		StringBuilder palavraMaiusculaSb = new StringBuilder();
		boolean palavraMaiusculaFormada = false;
		for (int i=0;i < listaNaoAjustada.size(); i++) {
			String s = listaNaoAjustada.get(i);
			if(this.isMaiuscula(s) && s.length() == 1){//Se letra maiuscula ou número
				if(criarPalavraMaiuscula(palavraMaiusculaSb, s, palavraMaiusculaFormada, i+1, listaNaoAjustada)){
					palavraMaiusculaFormada = palavraMaiusculaSb.length() > 1;
					continue;
				}
			}else{
				this.adicionarListaAjustada(palavraMaiusculaFormada, listaAjustada, palavraMaiusculaSb, s);
				palavraMaiusculaFormada = false;
			}
			this.addPalavraMaiuscula(palavraMaiusculaFormada, palavraMaiusculaSb, listaAjustada);
		}
		return listaAjustada;
	}

	
	private void addPalavraMaiuscula(boolean palavraMaiusculaFormada, StringBuilder palavraMaiusculaSb, List<String> listaAjustada){
		if(palavraMaiusculaFormada){
			listaAjustada.add(palavraMaiusculaSb.toString());
			palavraMaiusculaSb.setLength(0);
		}
	}
	
	private boolean criarPalavraMaiuscula(StringBuilder palavraMaiusculaSb, String s, boolean palavraMaiusculaFormada, int index, List<String> listaNaoAjustada){
		palavraMaiusculaSb.append(s);
		if(index < listaNaoAjustada.size()){
			return true;
		}
		return false;

	}

	private void adicionarListaAjustada(boolean palavraMaiusculaFormada, List<String> listaAjustada, StringBuilder palavraMaiusculaSb, String s){
		if(palavraMaiusculaFormada){
			listaAjustada.add(palavraMaiusculaSb.toString());
			listaAjustada.add(s.toLowerCase());
			palavraMaiusculaSb.setLength(0);

		}else{
			listaAjustada.add(s.toLowerCase());
		}

	}

	private boolean isMaiuscula(String letra){
		return letra.equals(letra.toUpperCase());
	}
}
