package br.com.pupposoft.tdd.semana2;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import br.com.pupposoft.tdd.semana2.entidades.Usuario;
import br.com.pupposoft.tdd.semana2.exceptions.UsuarioComNomeVazioException;
import br.com.pupposoft.tdd.semana2.exceptions.UsuarioInexistenteException;
import br.com.pupposoft.tdd.semana2.exceptions.UsuarioJaRegistradoException;

public class UsuarioServiceImplTest {

	private UsuarioServiceImpl usuarioServiceImpl;
	
	@Before
	public void setup(){
		this.usuarioServiceImpl = new UsuarioServiceImpl();
	}
	
	@Test
	public void registraUsuario_registraUsuarioSucesso() throws UsuarioJaRegistradoException, UsuarioComNomeVazioException, UsuarioInexistenteException {
		this.usuarioServiceImpl.registraUsuario("NomeUsuario");
		List<Usuario> usuarios = this.usuarioServiceImpl.get_usuarios();
		
		assertEquals(1, usuarios.size());
		assertEquals("NomeUsuario", usuarios.get(0).getNome());
	}

	@Test(expected=UsuarioJaRegistradoException.class)
	public void registraUsuario_usuarioJaRegistrado() throws UsuarioJaRegistradoException, UsuarioComNomeVazioException, UsuarioInexistenteException {
		this.usuarioServiceImpl.registraUsuario("NomeUsuario");
		
		
		try {
			this.usuarioServiceImpl.registraUsuario("NomeUsuario");
			
		} catch (UsuarioJaRegistradoException e) {
			List<Usuario> usuarios = this.usuarioServiceImpl.get_usuarios();
			assertEquals(1, usuarios.size());
			assertEquals("NomeUsuario", usuarios.get(0).getNome());
			assertEquals("Já existe usuário com o nome NomeUsuario",e.getMessage());
			
			throw e;
		}
		
	}
	
	@Test(expected=UsuarioComNomeVazioException.class)
	public void registraUsuario_usuarioNomeVazio() throws UsuarioJaRegistradoException, UsuarioComNomeVazioException, UsuarioInexistenteException {
		try {
			this.usuarioServiceImpl.registraUsuario("");
			
		} catch (UsuarioComNomeVazioException e) {
			List<Usuario> usuarios = this.usuarioServiceImpl.get_usuarios();
			assertEquals(0, usuarios.size());
			assertEquals("Não pode registrar usuario com nome vazio!",e.getMessage());
			
			throw e;
		}
		
	}
	
	@Test(expected=UsuarioInexistenteException.class)
	public void registraUsuario_usuarioInexistente() throws UsuarioJaRegistradoException, UsuarioComNomeVazioException, UsuarioInexistenteException {
		try {
			this.usuarioServiceImpl.registraUsuario(null);
			
		} catch (UsuarioComNomeVazioException e) {
			List<Usuario> usuarios = this.usuarioServiceImpl.get_usuarios();
			assertEquals(0, usuarios.size());
			assertEquals("Não pode registrar usuários inexistente!",e.getMessage());
			
			throw e;
		}
		
	}
}
