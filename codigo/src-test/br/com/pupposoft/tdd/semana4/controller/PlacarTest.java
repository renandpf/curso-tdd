package br.com.pupposoft.tdd.semana4.controller;

import org.junit.Test;
import static org.junit.Assert.*;

import java.util.List;

import br.com.pupposoft.tdd.semana4.enums.TipoPontoEnum;
import br.com.pupposoft.tdd.semana4.exceptions.PlacarException;
import br.com.pupposoft.tdd.semana4.service.mocks.PlacarServiceGetPontosUsuarioSucessoMock;
import br.com.pupposoft.tdd.semana4.service.mocks.PlacarServiceGetRankingSucessoMock;
import br.com.pupposoft.tdd.semana4.service.mocks.PlacarServiceExceptionMock;
import br.com.pupposoft.tdd.semana4.service.mocks.PlacarServiceRegistrarPontoSucessoMock;



public class PlacarTest {
	
	private Placar placarController;
	
	@Test
	public void registrarPonto_sucesso(){
		final String mensagemEsperada = "o usuário guerra recebeu 10 pontos do tipo estrela";
		
		int idUsuario = 1;
		TipoPontoEnum tipoPonto = TipoPontoEnum.ESTRELA;
		int quantidadePontos = 10;
		
		
		this.placarController = new Placar(new PlacarServiceRegistrarPontoSucessoMock("guerra", 1));
		String mensagemRetornada = this.placarController.registrarPonto(idUsuario, "guerra", tipoPonto, quantidadePontos);
		
		assertEquals(mensagemEsperada, mensagemRetornada);
	}

	@Test(expected=PlacarException.class)
	public void registrarPonto_falha(){
		final String mensagemEsperada = "Teste de erro ao registrar ponto";
		
		try {
			this.placarController = new Placar(new PlacarServiceExceptionMock());
			this.placarController.registrarPonto(0, null, null, 0);
		} catch (Exception e) {
			assertEquals(mensagemEsperada, e.getMessage());
			throw e;
		}
		
	}

	@Test
	public void getPontosUsuario_sucesso(){
		final String mensagemEsperada_01 = "20 pontos do tipo moeda";
		final String mensagemEsperada_02 = "25 pontos do tipo estrela";
		
		int idUsuario = 1;
		
		this.placarController = new Placar(new PlacarServiceGetPontosUsuarioSucessoMock());
		List<String> mensagens = this.placarController.getPontosUsuario(idUsuario);
		
		assertNotNull(mensagens);
		assertEquals(2, mensagens.size());
		assertEquals(mensagemEsperada_01, mensagens.get(0));
		assertEquals(mensagemEsperada_02, mensagens.get(1));
	}
	
	@Test(expected=PlacarException.class)
	public void getPontosUsuario_falha(){
		final String mensagemEsperada = "Teste de erro ao obter pontos";
		
		try {
			this.placarController = new Placar(new PlacarServiceExceptionMock());
			this.placarController.getPontosUsuario(0);
		} catch (Exception e) {
			assertEquals(mensagemEsperada, e.getMessage());
			throw e;
		}
	}
	
	@Test
	public void getRanking_sucesso(){
		final String mensagemEsperada_01 = "guerra com 25";
		final String mensagemEsperada_02 = "fernandes com 19";
		final String mensagemEsperada_03 = "rodrigo com 17";
		
		this.placarController = new Placar(new PlacarServiceGetRankingSucessoMock());
		List<String> mensagens = this.placarController.getRanking(TipoPontoEnum.MOEDA);
		
		assertNotNull(mensagens);
		assertEquals(3, mensagens.size());
		assertEquals(mensagemEsperada_01, mensagens.get(0));
		assertEquals(mensagemEsperada_02, mensagens.get(1));
		assertEquals(mensagemEsperada_03, mensagens.get(2));
	}
	
	@Test(expected=PlacarException.class)
	public void getRanking_falha(){
		final String mensagemEsperada = "Teste de erro ao obter ranking";
		
		try {
			this.placarController = new Placar(new PlacarServiceExceptionMock());
			this.placarController.getRanking(null);
		} catch (Exception e) {
			assertEquals(mensagemEsperada, e.getMessage());
			throw e;
		}
	}
}
