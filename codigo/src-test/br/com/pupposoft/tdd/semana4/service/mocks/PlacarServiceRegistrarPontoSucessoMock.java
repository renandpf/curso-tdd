package br.com.pupposoft.tdd.semana4.service.mocks;

import java.util.List;

import br.com.pupposoft.tdd.semana4.entidades.Ponto;
import br.com.pupposoft.tdd.semana4.entidades.Usuario;
import br.com.pupposoft.tdd.semana4.enums.TipoPontoEnum;
import br.com.pupposoft.tdd.semana4.service.IPlacarService;

public class PlacarServiceRegistrarPontoSucessoMock implements IPlacarService {

	private String nomeUsuario;
	private int idUsuario;
	
	public PlacarServiceRegistrarPontoSucessoMock(String nomeUsuario, int idUsuario) {
		super();
		this.nomeUsuario = nomeUsuario;
		this.idUsuario = idUsuario;
	}

	@Override
	public Ponto registrarPonto(Usuario usuario, TipoPontoEnum tipoPonto, Integer quantidade) {

		Ponto ponto = new Ponto();
		ponto.setTipo(tipoPonto);
		ponto.setQuantidade(quantidade);
		ponto.setUsuario(usuario);
		usuario.setId(idUsuario);
		usuario.setNome(nomeUsuario);
		
		
		return ponto;
	}

	@Override
	public List<Ponto> getPontosUsuario(Usuario usuario) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Ponto> getRanking(TipoPontoEnum tipoPonto) {
		// TODO Auto-generated method stub
		return null;
	}

}
