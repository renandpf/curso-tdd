package br.com.pupposoft.tdd.semana4.service.mocks;

import java.util.Arrays;
import java.util.List;

import br.com.pupposoft.tdd.semana4.entidades.Ponto;
import br.com.pupposoft.tdd.semana4.entidades.Usuario;
import br.com.pupposoft.tdd.semana4.enums.TipoPontoEnum;
import br.com.pupposoft.tdd.semana4.exceptions.PlacarException;
import br.com.pupposoft.tdd.semana4.service.IPlacarService;

public class PlacarServiceGetPontosUsuarioSucessoMock implements IPlacarService {

	@Override
	public Ponto registrarPonto(Usuario usuario, TipoPontoEnum tipoPonto, Integer quantidade) throws PlacarException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Ponto> getPontosUsuario(Usuario usuario) throws PlacarException {
		List<Ponto> pontos = Arrays.asList(new Ponto(), new Ponto());

		pontos.get(0).setTipo(TipoPontoEnum.MOEDA);
		pontos.get(0).setQuantidade(20);
		pontos.get(1).setTipo(TipoPontoEnum.ESTRELA);
		pontos.get(1).setQuantidade(25);
		
		return pontos;
	}

	@Override
	public List<Ponto> getRanking(TipoPontoEnum tipoPonto) throws PlacarException {
		// TODO Auto-generated method stub
		return null;
	}

}
