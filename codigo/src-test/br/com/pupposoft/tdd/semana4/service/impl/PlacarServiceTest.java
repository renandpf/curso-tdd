package br.com.pupposoft.tdd.semana4.service.impl;

import org.junit.Test;
import static org.junit.Assert.*;

import java.util.List;

import br.com.pupposoft.tdd.semana4.dao.mocks.ArmazanamentoDaoErroMock;
import br.com.pupposoft.tdd.semana4.dao.mocks.ArmazanamentoDaoSucessoMock;
import br.com.pupposoft.tdd.semana4.dao.mocks.ArmazanamentoDaoSucessoUmTipoPontoMock;
import br.com.pupposoft.tdd.semana4.entidades.Ponto;
import br.com.pupposoft.tdd.semana4.entidades.Usuario;
import br.com.pupposoft.tdd.semana4.enums.TipoPontoEnum;
import br.com.pupposoft.tdd.semana4.exceptions.PlacarException;
import br.com.pupposoft.tdd.semana4.service.IPlacarService;

public class PlacarServiceTest {
	
	private IPlacarService placarService;
	
	@Test
	public void registrarPonto_sucesso(){
		String nomeEsperado = "Guerra";
		
		Usuario usuario = new Usuario();
		TipoPontoEnum tipoPonto = TipoPontoEnum.ENERGIA;
		int quantidade = 15;
		
		
		this.placarService = new PlacarService(new ArmazanamentoDaoSucessoMock());
		Ponto pontoRegistrado = this.placarService.registrarPonto(usuario, tipoPonto, quantidade);
		
		assertNotNull(pontoRegistrado);
		assertEquals(nomeEsperado,pontoRegistrado.getUsuario().getNome());
		assertEquals(tipoPonto,pontoRegistrado.getTipo());
		assertEquals(quantidade,pontoRegistrado.getQuantidade(),0);
	}

	@Test(expected=PlacarException.class)
	public void registrarPonto_erro(){
		final String mensagemEsperada = "Ocorreu um erro ao registrar o ponto";
		this.placarService = new PlacarService(new ArmazanamentoDaoErroMock());
		
		try {
			this.placarService.registrarPonto(null, null, null);
			
		} catch (Exception e) {
			assertEquals(mensagemEsperada,e.getMessage());
			assertNotNull(e.getSuppressed());
			throw e;
		}
	}
	
	
	@Test
	public void getPontosUsuario_sucesso(){
		
		this.placarService = new PlacarService(new ArmazanamentoDaoSucessoMock());
		
		
		List<Ponto> pontos = this.placarService.getPontosUsuario(new Usuario());

		assertNotNull(pontos);
		assertEquals(3, pontos.size());
		assertEquals(15, pontos.get(0).getQuantidade(),0);
		assertEquals(TipoPontoEnum.ENERGIA, pontos.get(0).getTipo());
		assertEquals(20, pontos.get(1).getQuantidade(),0);
		assertEquals(TipoPontoEnum.ESTRELA, pontos.get(1).getTipo());
		assertEquals(25, pontos.get(2).getQuantidade(),0);
		assertEquals(TipoPontoEnum.MOEDA, pontos.get(2).getTipo());
	}
	
	@Test(expected=PlacarException.class)
	public void getPontosUsuario_erro(){
		final String mensagemEsperada = "Ocorreu um erro ao obter os pontos do usuário";
		this.placarService = new PlacarService(new ArmazanamentoDaoErroMock());
		
		try {
			this.placarService.getPontosUsuario(null);
			
		} catch (Exception e) {
			assertEquals(mensagemEsperada,e.getMessage());
			assertNotNull(e.getSuppressed());
			throw e;
		}
	}	
	
	@Test
	public void getRanking_sucesso(){
		this.placarService = new PlacarService(new ArmazanamentoDaoSucessoUmTipoPontoMock());
		
		List<Ponto> pontos = this.placarService.getRanking(TipoPontoEnum.ENERGIA);
		
		assertNotNull(pontos);
		assertEquals(3, pontos.size());
		assertEquals(25, pontos.get(0).getQuantidade(),0);
		assertEquals(25, pontos.get(0).getUsuario().getId(),0);
		assertEquals(TipoPontoEnum.ENERGIA, pontos.get(0).getTipo());
		
		assertEquals(20, pontos.get(1).getQuantidade(),0);
		assertEquals(TipoPontoEnum.ENERGIA, pontos.get(1).getTipo());
		assertEquals(20, pontos.get(1).getUsuario().getId(),0);

		assertEquals(15, pontos.get(2).getQuantidade(),0);
		assertEquals(TipoPontoEnum.ENERGIA, pontos.get(2).getTipo());
		assertEquals(15, pontos.get(2).getUsuario().getId(),0);
	}

	@Test(expected=PlacarException.class)
	public void getRanking_erro(){
		final String mensagemEsperada = "Ocorreu um erro ao obter o ranking do tipo de ponto";
		this.placarService = new PlacarService(new ArmazanamentoDaoErroMock());
		
		try {
			this.placarService.getRanking(null);
			
		} catch (Exception e) {
			assertEquals(mensagemEsperada,e.getMessage());
			assertNotNull(e.getSuppressed());
			throw e;
		}
	}	
	
	
}
