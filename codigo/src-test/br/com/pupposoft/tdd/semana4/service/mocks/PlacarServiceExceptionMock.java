package br.com.pupposoft.tdd.semana4.service.mocks;

import java.util.List;

import br.com.pupposoft.tdd.semana4.entidades.Ponto;
import br.com.pupposoft.tdd.semana4.entidades.Usuario;
import br.com.pupposoft.tdd.semana4.enums.TipoPontoEnum;
import br.com.pupposoft.tdd.semana4.exceptions.PlacarException;
import br.com.pupposoft.tdd.semana4.service.IPlacarService;

public class PlacarServiceExceptionMock implements IPlacarService {

	
	@Override
	public Ponto registrarPonto(Usuario usuario, TipoPontoEnum tipoPonto, Integer quantidade) {
		throw new PlacarException("Teste de erro ao registrar ponto");
	}

	@Override
	public List<Ponto> getPontosUsuario(Usuario usuario) {
		throw new PlacarException("Teste de erro ao obter pontos");
	}

	@Override
	public List<Ponto> getRanking(TipoPontoEnum tipoPonto) {
		throw new PlacarException("Teste de erro ao obter ranking");
	}

}
