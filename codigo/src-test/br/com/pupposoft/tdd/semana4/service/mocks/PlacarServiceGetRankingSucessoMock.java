package br.com.pupposoft.tdd.semana4.service.mocks;

import java.util.Arrays;
import java.util.List;

import br.com.pupposoft.tdd.semana4.entidades.Ponto;
import br.com.pupposoft.tdd.semana4.entidades.Usuario;
import br.com.pupposoft.tdd.semana4.enums.TipoPontoEnum;
import br.com.pupposoft.tdd.semana4.exceptions.PlacarException;
import br.com.pupposoft.tdd.semana4.service.IPlacarService;

public class PlacarServiceGetRankingSucessoMock implements IPlacarService {

	@Override
	public Ponto registrarPonto(Usuario usuario, TipoPontoEnum tipoPonto, Integer quantidade) throws PlacarException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Ponto> getPontosUsuario(Usuario usuario) throws PlacarException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Ponto> getRanking(TipoPontoEnum tipoPonto) throws PlacarException {

		List<Usuario> usuarios = Arrays.asList(new Usuario(), new Usuario(), new Usuario());
		List<Ponto> pontos = Arrays.asList(new Ponto(), new Ponto(), new Ponto());

		pontos.get(0).setTipo(TipoPontoEnum.MOEDA);
		pontos.get(0).setQuantidade(25);
		pontos.get(0).setUsuario(usuarios.get(0));
		pontos.get(1).setTipo(TipoPontoEnum.MOEDA);
		pontos.get(1).setQuantidade(19);
		pontos.get(1).setUsuario(usuarios.get(1));
		pontos.get(2).setTipo(TipoPontoEnum.MOEDA);
		pontos.get(2).setQuantidade(17);
		pontos.get(2).setUsuario(usuarios.get(2));
		
		
		usuarios.get(0).setNome("guerra");
		usuarios.get(1).setNome("fernandes");
		usuarios.get(2).setNome("rodrigo");
		
		return pontos;
	}

}
