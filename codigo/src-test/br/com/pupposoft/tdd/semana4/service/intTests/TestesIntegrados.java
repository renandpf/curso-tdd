package br.com.pupposoft.tdd.semana4.service.intTests;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import br.com.pupposoft.tdd.semana4.controller.Placar;
import br.com.pupposoft.tdd.semana4.dao.IArmazenamentoDao;
import br.com.pupposoft.tdd.semana4.dao.impl.Armazenamento;
import br.com.pupposoft.tdd.semana4.enums.TipoPontoEnum;
import br.com.pupposoft.tdd.semana4.service.IPlacarService;
import br.com.pupposoft.tdd.semana4.service.impl.PlacarService;

/**
 * Classe que executa testes de ponta a ponta.
 * A execução desta classe pode afetar o resultados do testes unitários de DAO
 * 
 * @author renan.puppo@gmail.com
 *
 */
public class TestesIntegrados {

	private Placar placarController;
	
	@Before
	public void setup(){
		IArmazenamentoDao armazenamentoDao = new Armazenamento(null);//Se "null" utiliza a home do usuário
		IPlacarService placarService = new PlacarService(armazenamentoDao);
		this.placarController = new Placar(placarService);
	}
	
	@Test
	public void registrarPonto_IntTest(){
		System.out.println("### TESTE INTEGRADO - registrarPonto - INICIO ###");
		String mensagem = this.placarController.registrarPonto(1, "Nome User 3", TipoPontoEnum.ESTRELA, 150);
		System.out.println(mensagem);
		mensagem = this.placarController.registrarPonto(2, "Nome User 2", TipoPontoEnum.ENERGIA, 250);
		System.out.println(mensagem);
		System.out.println("### TESTE INTEGRADO - registrarPonto - FIM ###\n");
	}
	
	@Test
	public void getPontosUsuario_IntTest(){
		System.out.println("### TESTE INTEGRADO - getPontosUsuario - INICIO ###");
		List<String> mensagens = this.placarController.getPontosUsuario(1);		
		System.out.println(mensagens);
		
		mensagens = this.placarController.getPontosUsuario(2);		
		System.out.println(mensagens);
		System.out.println("### TESTE INTEGRADO - getPontosUsuario - FIM ###\n");
	}

	@Test
	public void getRanking_IntTest(){
		System.out.println("### TESTE INTEGRADO - getRanking - INICIO ###");
		List<String> mensagens = this.placarController.getRanking(TipoPontoEnum.ENERGIA);	
		System.out.println(mensagens);
		
		mensagens = this.placarController.getRanking(TipoPontoEnum.ESTRELA);		
		System.out.println(mensagens);
		System.out.println("### TESTE INTEGRADO - getRanking - FIM ###\n");
	}
	
	
}
