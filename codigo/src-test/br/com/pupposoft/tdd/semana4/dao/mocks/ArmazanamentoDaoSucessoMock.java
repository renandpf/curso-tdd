package br.com.pupposoft.tdd.semana4.dao.mocks;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import br.com.pupposoft.tdd.semana4.dao.IArmazenamentoDao;
import br.com.pupposoft.tdd.semana4.entidades.Ponto;
import br.com.pupposoft.tdd.semana4.entidades.Usuario;
import br.com.pupposoft.tdd.semana4.enums.TipoPontoEnum;

public class ArmazanamentoDaoSucessoMock implements IArmazenamentoDao {

	@Override
	public void registrarPonto(Ponto ponto) throws IOException {
		ponto.getUsuario().setNome("Guerra");
	}

	@Override
	public List<Ponto> pesquisarPontos(Usuario usuario, TipoPontoEnum tipoPonto) throws IOException {
		List<Ponto> pontos = Arrays.asList(new Ponto(), new Ponto(), new Ponto());
		usuario.setId(1);
		usuario.setPontos(pontos);
		
		pontos.get(0).setQuantidade(15);
		pontos.get(0).setTipo(TipoPontoEnum.ENERGIA);
		pontos.get(0).setUsuario(usuario);
		pontos.get(1).setQuantidade(20);
		pontos.get(1).setTipo(TipoPontoEnum.ESTRELA);
		pontos.get(1).setUsuario(usuario);
		pontos.get(2).setQuantidade(25);
		pontos.get(2).setTipo(TipoPontoEnum.MOEDA);
		pontos.get(2).setUsuario(usuario);
		
		return pontos;
	}

}
