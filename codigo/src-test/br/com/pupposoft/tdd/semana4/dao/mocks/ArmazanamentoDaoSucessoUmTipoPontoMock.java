package br.com.pupposoft.tdd.semana4.dao.mocks;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import br.com.pupposoft.tdd.semana4.dao.IArmazenamentoDao;
import br.com.pupposoft.tdd.semana4.entidades.Ponto;
import br.com.pupposoft.tdd.semana4.entidades.Usuario;
import br.com.pupposoft.tdd.semana4.enums.TipoPontoEnum;

public class ArmazanamentoDaoSucessoUmTipoPontoMock implements IArmazenamentoDao {

	@Override
	public void registrarPonto(Ponto ponto) throws IOException {
		// TODO Auto-generated method stub

	}

	@Override
	public List<Ponto> pesquisarPontos(Usuario usuario, TipoPontoEnum tipoPonto) throws IOException {
		List<Ponto> pontos = Arrays.asList(new Ponto(), new Ponto(), new Ponto());
		
		pontos.get(0).setQuantidade(15);
		pontos.get(0).setTipo(TipoPontoEnum.ENERGIA);
		pontos.get(0).setUsuario(new Usuario());
		pontos.get(0).getUsuario().setId(15);
		
		pontos.get(1).setQuantidade(25);
		pontos.get(1).setTipo(TipoPontoEnum.ENERGIA);
		pontos.get(1).setUsuario(new Usuario());
		pontos.get(1).getUsuario().setId(25);
		
		pontos.get(2).setQuantidade(20);
		pontos.get(2).setTipo(TipoPontoEnum.ENERGIA);
		pontos.get(2).setUsuario(new Usuario());
		pontos.get(2).getUsuario().setId(20);
		
		return pontos;
	}

}
