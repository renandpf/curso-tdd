package br.com.pupposoft.tdd.semana4.dao.impl;

import java.io.IOException;
import java.nio.file.AccessDeniedException;
import java.nio.file.Paths;
import java.util.List;

import org.junit.Test;
import static org.junit.Assert.*;

import br.com.pupposoft.tdd.semana4.dao.IArmazenamentoDao;
import br.com.pupposoft.tdd.semana4.entidades.Ponto;
import br.com.pupposoft.tdd.semana4.entidades.Usuario;
import br.com.pupposoft.tdd.semana4.enums.TipoPontoEnum;

public class ArmazenamentoTest {
	
	private IArmazenamentoDao armazenamentoDao;
	
	@Test
	public void registrarPontoEnergia_sucesso() throws IOException{
		armazenamentoDao = new Armazenamento(null);
		
		Ponto p = new Ponto();
		p.setQuantidade(20);
		p.setTipo(TipoPontoEnum.ENERGIA);
		p.setUsuario(new Usuario());
		p.getUsuario().setId(1);
		p.getUsuario().setNome("Nome 1");
		
		armazenamentoDao.registrarPonto(p);
		
	}

	@Test
	public void registrarPontoEstrela_sucesso() throws IOException{
		armazenamentoDao = new Armazenamento(null);
		
		Ponto p = new Ponto();
		p.setQuantidade(15);
		p.setTipo(TipoPontoEnum.ESTRELA);
		p.setUsuario(new Usuario());
		p.getUsuario().setId(1);
		p.getUsuario().setNome("Nome 1");
		
		armazenamentoDao.registrarPonto(p);
		
	}

	@Test(expected=IOException.class)
	public void registrarPonto_error() throws IOException{
		// "/" Necessita de acesso root e deve retornar java.nio.file.AccessDeniedException (que é um IOException)
		armazenamentoDao = new Armazenamento(Paths.get("/"));
		
		Ponto p = new Ponto();
		p.setQuantidade(15);
		p.setTipo(TipoPontoEnum.ESTRELA);
		p.setUsuario(new Usuario());
		p.getUsuario().setId(1);
		p.getUsuario().setNome("Nome 1");
		
		try {
			armazenamentoDao.registrarPonto(p);
			
		} catch (AccessDeniedException e) {
			throw e;
		}
	}

	@Test
	public void pesquisarPontos_porUsuario() throws IOException{
		armazenamentoDao = new Armazenamento(null);

		this.prepararTestePesquisa();
		
		Usuario usuario = new Usuario();
		usuario.setId(1);
		
		List<Ponto> pontos = armazenamentoDao.pesquisarPontos(usuario, null);
		
		assertNotNull(pontos);
		assertEquals(2, pontos.size());
		assertEquals(TipoPontoEnum.ESTRELA, pontos.get(0).getTipo());
		assertEquals(15, pontos.get(0).getQuantidade(),0);
		assertEquals("Nome 1", pontos.get(0).getUsuario().getNome());
		
		assertEquals(TipoPontoEnum.ENERGIA, pontos.get(1).getTipo());
		assertEquals(20, pontos.get(1).getQuantidade(),0);
		assertEquals("Nome 1", pontos.get(1).getUsuario().getNome());
	}
	
	@Test
	public void pesquisarPontos_porTipoPonto() throws IOException{
		armazenamentoDao = new Armazenamento(null);
		
		this.prepararTestePesquisa();
		
		List<Ponto> pontos = armazenamentoDao.pesquisarPontos(null, TipoPontoEnum.ESTRELA);
		
		assertNotNull(pontos);
		assertEquals(2, pontos.size());
		assertEquals(TipoPontoEnum.ESTRELA, pontos.get(0).getTipo());
		assertEquals(20, pontos.get(0).getQuantidade(),0);
		assertEquals("Nome 2", pontos.get(0).getUsuario().getNome());

		assertEquals(TipoPontoEnum.ESTRELA, pontos.get(1).getTipo());
		assertEquals(15, pontos.get(1).getQuantidade(),0);
		assertEquals("Nome 1", pontos.get(1).getUsuario().getNome());
	}		
	
	private void prepararTestePesquisa() throws IOException{
		Ponto p = new Ponto();
		p.setQuantidade(20);
		p.setTipo(TipoPontoEnum.ENERGIA);
		p.setUsuario(new Usuario());
		p.getUsuario().setId(1);
		p.getUsuario().setNome("Nome 1");
		
		armazenamentoDao.registrarPonto(p);

		p = new Ponto();
		p.setQuantidade(15);
		p.setTipo(TipoPontoEnum.ESTRELA);
		p.setUsuario(new Usuario());
		p.getUsuario().setId(1);
		p.getUsuario().setNome("Nome 1");
		
		armazenamentoDao.registrarPonto(p);
		
		p = new Ponto();
		p.setQuantidade(15);
		p.setTipo(TipoPontoEnum.ENERGIA);
		p.setUsuario(new Usuario());
		p.getUsuario().setId(2);
		p.getUsuario().setNome("Nome 2");
		
		armazenamentoDao.registrarPonto(p);
		
		p = new Ponto();
		p.setQuantidade(20);
		p.setTipo(TipoPontoEnum.ESTRELA);
		p.setUsuario(new Usuario());
		p.getUsuario().setId(2);
		p.getUsuario().setNome("Nome 2");
		
		armazenamentoDao.registrarPonto(p);
	}
	
	
}
