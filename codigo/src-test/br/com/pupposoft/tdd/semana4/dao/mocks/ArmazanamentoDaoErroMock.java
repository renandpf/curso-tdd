package br.com.pupposoft.tdd.semana4.dao.mocks;

import java.io.IOException;
import java.util.List;

import br.com.pupposoft.tdd.semana4.dao.IArmazenamentoDao;
import br.com.pupposoft.tdd.semana4.entidades.Ponto;
import br.com.pupposoft.tdd.semana4.entidades.Usuario;
import br.com.pupposoft.tdd.semana4.enums.TipoPontoEnum;

public class ArmazanamentoDaoErroMock implements IArmazenamentoDao {

	@Override
	public void registrarPonto(Ponto ponto) throws IOException {
		throw new IOException();
	}

	@Override
	public List<Ponto> pesquisarPontos(Usuario usuario, TipoPontoEnum tipoPonto) throws IOException {
		throw new IOException();
	}

}
