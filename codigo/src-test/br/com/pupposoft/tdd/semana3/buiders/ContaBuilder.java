package br.com.pupposoft.tdd.semana3.buiders;

import java.util.ArrayList;

import br.com.pupposoft.tdd.semana3.entities.ContaCorrente;

public class ContaBuilder {

	
	public static ContaCorrente getContaCorrente(){
		ContaCorrente cc = new ContaCorrente();
		cc.setNumeroContaCorrente("123");
		cc.setMovimentos(new ArrayList<>());
		
		return cc;
	}
}
