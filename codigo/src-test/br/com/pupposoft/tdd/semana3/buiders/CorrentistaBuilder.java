package br.com.pupposoft.tdd.semana3.buiders;

import java.util.ArrayList;

import br.com.pupposoft.tdd.semana3.entities.Correntista;

public class CorrentistaBuilder {
	
	public static Correntista getCorrentista(){
		Correntista correntista = new Correntista();
		correntista.setUsuario("user");
		correntista.setSenha("senha");
		correntista.setContasCorrentes(new ArrayList<>());
		
		return correntista;
	}

}
