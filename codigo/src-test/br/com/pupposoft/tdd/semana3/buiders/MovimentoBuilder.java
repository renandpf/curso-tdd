package br.com.pupposoft.tdd.semana3.buiders;

import java.time.LocalDateTime;
import java.time.Month;
import java.util.Arrays;
import java.util.List;

import br.com.pupposoft.tdd.semana3.entities.Movimento;

public class MovimentoBuilder {

	public static List<Movimento> getMovimentos(){
		List<Movimento> movimentos = Arrays.asList(new Movimento(), new Movimento(), new Movimento());
		
		movimentos.get(0).setDescricaoResumida("Movimento A");
		movimentos.get(0).setValor(100);
		movimentos.get(0).setDataHora(LocalDateTime.of(2016, Month.JANUARY, 1, 8, 0,0));
		
		movimentos.get(1).setDescricaoResumida("Movimento B");
		movimentos.get(1).setValor(200);
		movimentos.get(1).setDataHora(LocalDateTime.of(2016, Month.JANUARY, 2, 8, 0,0));

		movimentos.get(2).setDescricaoResumida("Movimento C");
		movimentos.get(2).setValor(300);
		movimentos.get(2).setDataHora(LocalDateTime.of(2016, Month.JANUARY, 3, 8, 0,0));
		
		
		return movimentos;
	}
	
}
