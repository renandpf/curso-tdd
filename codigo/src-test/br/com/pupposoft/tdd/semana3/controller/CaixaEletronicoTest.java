package br.com.pupposoft.tdd.semana3.controller;

import org.junit.Before;
import org.junit.Test;

import br.com.pupposoft.tdd.semana3.buiders.ContaBuilder;
import br.com.pupposoft.tdd.semana3.buiders.CorrentistaBuilder;
import br.com.pupposoft.tdd.semana3.buiders.MovimentoBuilder;
import br.com.pupposoft.tdd.semana3.entities.ContaCorrente;
import br.com.pupposoft.tdd.semana3.entities.Correntista;
import br.com.pupposoft.tdd.semana3.exceptions.HardwareException;
import br.com.pupposoft.tdd.semana3.helper.GlobalMessage;
import br.com.pupposoft.tdd.semana3.services.Hardware;
import br.com.pupposoft.tdd.semana3.services.ServicoRemoto;
import br.com.pupposoft.tdd.semana3.services.mocks.HardwareMock;
import br.com.pupposoft.tdd.semana3.services.mocks.ServicoRemotoMock;

import static org.junit.Assert.*;

import java.util.ArrayList;

public class CaixaEletronicoTest {

	@Before
	public void setup(){
	}

	@Test
	public void logar_sucesso() throws HardwareException{
		String usuario = "user";
		String senha = "senha";

		ContaCorrente cc = ContaBuilder.getContaCorrente();
		Correntista c = CorrentistaBuilder.getCorrentista();
		cc.setCorrentista(c);
		c.getContasCorrentes().add(cc);

		Hardware hardware = new HardwareMock("1122",false,false,false);
		ServicoRemoto servicoRemoto = new ServicoRemotoMock(cc);
		CaixaEletronico caixaEletronico = new CaixaEletronico(hardware,servicoRemoto);

		String mensagem = caixaEletronico.logar(usuario, senha);

		assertEquals(GlobalMessage.LOGIN_SUCESSO, mensagem);
	}

	@Test
	public void logar_erro_contaDestinoNaoEncontrada() throws HardwareException{
		Hardware hardware = new HardwareMock("1122",false,false,false);
		ServicoRemoto servicoRemoto = new ServicoRemotoMock(null);
		CaixaEletronico caixaEletronico = new CaixaEletronico(hardware,servicoRemoto);

		String mensagemRetornada = caixaEletronico.logar("user", "senha");
		assertEquals(GlobalMessage.CONTA_NAO_ENCONTRADA, mensagemRetornada);
	}	

	@Test
	public void logar_falhaAutenticacao() throws HardwareException{
		String usuario = "user";
		String senha = "senhaErrada";

		ContaCorrente cc = ContaBuilder.getContaCorrente();
		Correntista c = CorrentistaBuilder.getCorrentista();
		cc.setCorrentista(c);
		c.getContasCorrentes().add(cc);

		Hardware hardware = new HardwareMock("1122",false,false,false);
		ServicoRemoto servicoRemoto = new ServicoRemotoMock(cc);
		CaixaEletronico caixaEletronico = new CaixaEletronico(hardware,servicoRemoto);

		String mensagem = caixaEletronico.logar(usuario, senha);

		assertEquals(GlobalMessage.LOGIN_ERRO, mensagem);
	}

	@Test
	public void logar_hardwareException() throws HardwareException{
		String usuario = "user";
		String senha = "senha";

		ContaCorrente cc = ContaBuilder.getContaCorrente();
		Correntista c = CorrentistaBuilder.getCorrentista();
		cc.setCorrentista(c);
		c.getContasCorrentes().add(cc);

		Hardware hardware = new HardwareMock("1122",true,false,false);
		ServicoRemoto servicoRemoto = new ServicoRemotoMock(cc);
		CaixaEletronico caixaEletronico = new CaixaEletronico(hardware,servicoRemoto);

		String mensagem = caixaEletronico.logar(usuario, senha);
		assertEquals(GlobalMessage.LOGIN_ERRO, mensagem);
	}

	@Test
	public void saldo_sucesso() throws HardwareException{
		final double saldoEsperado = 600;
		final String mensagemEsperada = GlobalMessage.SALDO+saldoEsperado;

		ContaCorrente cc = ContaBuilder.getContaCorrente();
		Correntista c = CorrentistaBuilder.getCorrentista();
		cc.setCorrentista(c);
		c.getContasCorrentes().add(cc);

		cc.setMovimentos(MovimentoBuilder.getMovimentos());

		Hardware hardware = new HardwareMock("1122",false,false,false);
		ServicoRemoto servicoRemoto = new ServicoRemotoMock(cc);
		CaixaEletronico caixaEletronico = new CaixaEletronico(hardware,servicoRemoto);

		caixaEletronico.logar("usuario", "senha");
		String mensagem = caixaEletronico.saldo();
		assertEquals(mensagemEsperada, mensagem);
	}

	@Test
	public void deposito_sucesso() throws HardwareException{
		String numeroContaDestinoEsperado = "123";
		double valorDepositoEsperado = 130;
		String descricaoEsperada = "Teste Depósito";

		ContaCorrente cc = ContaBuilder.getContaCorrente();
		Correntista c = CorrentistaBuilder.getCorrentista();
		cc.setCorrentista(c);
		c.getContasCorrentes().add(cc);

		cc.setMovimentos(new  ArrayList<>(MovimentoBuilder.getMovimentos()));

		Hardware hardware = new HardwareMock("1122",false,false,false);
		ServicoRemoto servicoRemoto = new ServicoRemotoMock(cc);
		CaixaEletronico caixaEletronico = new CaixaEletronico(hardware,servicoRemoto);

		String mensagemRetornada = caixaEletronico.depositar(numeroContaDestinoEsperado, valorDepositoEsperado, descricaoEsperada);

		assertEquals(GlobalMessage.DEPOSITO_SUCESSO, mensagemRetornada);
	}

	@Test
	public void deposito_erro_HardwareException() throws HardwareException{
		ContaCorrente cc = ContaBuilder.getContaCorrente();
		Correntista c = CorrentistaBuilder.getCorrentista();
		cc.setCorrentista(c);
		c.getContasCorrentes().add(cc);

		cc.setMovimentos(new  ArrayList<>(MovimentoBuilder.getMovimentos()));

		Hardware hardware = new HardwareMock("1122",false,false,true);
		ServicoRemoto servicoRemoto = new ServicoRemotoMock(cc);
		CaixaEletronico caixaEletronico = new CaixaEletronico(hardware,servicoRemoto);

		String mensagemRetornada = caixaEletronico.depositar("", 1, "");

		assertEquals(GlobalMessage.ERRO_HARDWARE, mensagemRetornada);
	}

	@Test
	public void deposito_erro_contaDestinoNaoEncontrada(){
		ContaCorrente cc = null;

		Hardware hardware = new HardwareMock("1122",false,false,false);
		ServicoRemoto servicoRemoto = new ServicoRemotoMock(cc);
		CaixaEletronico caixaEletronico = new CaixaEletronico(hardware,servicoRemoto);

		String mensagemRetornada = caixaEletronico.depositar("", 1, "");

		assertEquals(GlobalMessage.CONTA_NAO_ENCONTRADA, mensagemRetornada);
	}


	@Test
	public void sacar_sucesso() throws HardwareException{
		double valorSaque = -600;

		ContaCorrente cc = ContaBuilder.getContaCorrente();
		Correntista c = CorrentistaBuilder.getCorrentista();
		cc.setCorrentista(c);
		c.getContasCorrentes().add(cc);

		cc.setMovimentos(new  ArrayList<>(MovimentoBuilder.getMovimentos()));

		Hardware hardware = new HardwareMock("1122",false,false,true);
		ServicoRemoto servicoRemoto = new ServicoRemotoMock(cc);
		CaixaEletronico caixaEletronico = new CaixaEletronico(hardware,servicoRemoto);

		String mensagemRetornada = caixaEletronico.sacar(valorSaque);

		assertEquals(GlobalMessage.SAQUE_SUCESSO, mensagemRetornada);
	}	

	@Test
	public void sacar_erro_contaNaoEncontrada() throws HardwareException{
		double valorSaque = 600;

		ContaCorrente cc = null;

		Hardware hardware = new HardwareMock("1122",false,false,true);
		ServicoRemoto servicoRemoto = new ServicoRemotoMock(cc);
		CaixaEletronico caixaEletronico = new CaixaEletronico(hardware,servicoRemoto);

		String mensagem = caixaEletronico.sacar(valorSaque);

		assertEquals(GlobalMessage.CONTA_NAO_ENCONTRADA, mensagem);
	}

	@Test
	public void sacar_erro_hardwareException() throws HardwareException{
		double valorSaque = 600;

		ContaCorrente cc = ContaBuilder.getContaCorrente();

		Hardware hardware = new HardwareMock("1122",true,false,false);
		ServicoRemoto servicoRemoto = new ServicoRemotoMock(cc);
		CaixaEletronico caixaEletronico = new CaixaEletronico(hardware,servicoRemoto);

		String mensagem = caixaEletronico.sacar(valorSaque);

		assertEquals(GlobalMessage.ERRO_HARDWARE, mensagem);
	}	

	@Test
	public void sacar_erro_saldoInsuficiente() throws HardwareException{
		double valorSaque = 600.01;

		ContaCorrente cc = ContaBuilder.getContaCorrente();
		Correntista c = CorrentistaBuilder.getCorrentista();
		cc.setCorrentista(c);
		c.getContasCorrentes().add(cc);

		cc.setMovimentos(new  ArrayList<>(MovimentoBuilder.getMovimentos()));

		Hardware hardware = new HardwareMock("1122",false,false,false);
		ServicoRemoto servicoRemoto = new ServicoRemotoMock(cc);
		CaixaEletronico caixaEletronico = new CaixaEletronico(hardware,servicoRemoto);

		String mensagem = caixaEletronico.sacar(valorSaque);

		assertEquals(GlobalMessage.SALDO_INSUFICIENTE, mensagem);
	}	
}
