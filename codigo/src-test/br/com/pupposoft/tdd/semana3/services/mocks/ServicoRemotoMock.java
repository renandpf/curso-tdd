package br.com.pupposoft.tdd.semana3.services.mocks;

import br.com.pupposoft.tdd.semana3.entities.ContaCorrente;
import br.com.pupposoft.tdd.semana3.services.ServicoRemoto;

public class ServicoRemotoMock implements ServicoRemoto{

	private ContaCorrente contaCorrente;
	
	public ServicoRemotoMock(ContaCorrente contaCorrente) {
		this.contaCorrente = contaCorrente;
	}

	@Override
	public ContaCorrente recuperarConta(String numeroConta) {
		return this.contaCorrente;
	}

	@Override
	public void persistirConta(ContaCorrente contaCorrente) {
		// TODO Auto-generated method stub
		
	}

}
