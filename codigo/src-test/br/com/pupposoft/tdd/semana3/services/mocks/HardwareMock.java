package br.com.pupposoft.tdd.semana3.services.mocks;

import br.com.pupposoft.tdd.semana3.exceptions.HardwareException;
import br.com.pupposoft.tdd.semana3.services.Hardware;

public class HardwareMock implements Hardware{

	private String numeroCartao;
	private boolean gerarExceptionQuandoPegarNumeroDaContaCartao;
	private boolean gerarExceptionQuandoEntregarDinheiro;
	private boolean gerarExceptionQuandolerEnvelope;
	
	public HardwareMock(String numeroCartao, 
			boolean gerarExceptionQuandoPegarNumeroDaContaCartao,
			boolean gerarExceptionQuandoEntregarDinheiro,
			boolean gerarExceptionQuandolerEnvelope) {
		this.numeroCartao = numeroCartao;
		this.gerarExceptionQuandoPegarNumeroDaContaCartao = gerarExceptionQuandoPegarNumeroDaContaCartao;
		this.gerarExceptionQuandoEntregarDinheiro = gerarExceptionQuandoEntregarDinheiro;
		this.gerarExceptionQuandolerEnvelope = gerarExceptionQuandolerEnvelope;
	}

	@Override
	public String pegarNumeroDaContaCartao() throws HardwareException {
		if(gerarExceptionQuandoPegarNumeroDaContaCartao){
			throw new HardwareException();
		}
		
		return this.numeroCartao;
	}

	@Override
	public void entregarDinheiro() throws HardwareException {
		if(gerarExceptionQuandoEntregarDinheiro){
			throw new HardwareException();
		}
		
	}

	@Override
	public void lerEnvelope() throws HardwareException {
		if(gerarExceptionQuandolerEnvelope){
			throw new HardwareException();
		}
	}
}
