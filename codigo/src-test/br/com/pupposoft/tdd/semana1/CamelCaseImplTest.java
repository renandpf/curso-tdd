package br.com.pupposoft.tdd.semana1;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import br.com.pupposoft.tdd.semana1.exceptions.RegraNegocioException;

public class CamelCaseImplTest {

	private CamelCaseImpl ccImpl;
	
	@Before
	public void setup(){
		this.ccImpl = new CamelCaseImpl();
	}
	
	@Test
	public void toList_COM_UMA_PALAVRA_MINUSCULA() {
		final List<String> listaEsperada = Arrays.asList("nome");
		
		String camelCase = "nome";
		List<String> listaRetornada = ccImpl.toList(camelCase);
		
		assertEquals(listaEsperada.get(0), listaRetornada.get(0));
	}

	@Test
	public void toList_COM_UMA_PALAVRA_INICIANDO_MAIUSCULA() {
		final List<String> listaEsperada = Arrays.asList("nome");
		
		String camelCase = "Nome";
		List<String> listaRetornada = ccImpl.toList(camelCase);
		
		assertEquals(listaEsperada.get(0), listaRetornada.get(0));
	}
	
	@Test
	public void toList_NOME_COMPOSTO() {
		final List<String> listaEsperada = Arrays.asList("nome","composto");
		
		String camelCase = "nomeComposto";
		List<String> listaRetornada = ccImpl.toList(camelCase);
		
		assertNotNull(listaRetornada);
		assertEquals(listaEsperada.size(),listaRetornada.size());
		assertEquals(listaEsperada.get(0), listaRetornada.get(0));
		assertEquals(listaEsperada.get(1), listaRetornada.get(1));
	}
	
	@Test
	public void toList_NOME_COMPOSTO_COMECANDO_MAIUSCULO() {
		final List<String> listaEsperada = Arrays.asList("nome","composto");
		
		String camelCase = "NomeComposto";
		List<String> listaRetornada = ccImpl.toList(camelCase);
		
		assertNotNull(listaRetornada);
		assertEquals(listaEsperada.size(),listaRetornada.size());
		assertEquals(listaEsperada.get(0), listaRetornada.get(0));
		assertEquals(listaEsperada.get(1), listaRetornada.get(1));
	}
	
	
	@Test
	public void toList_PALAVRAS_TODAS_MAIUSCULAS() {
		
		final List<String> listaEsperada = Arrays.asList("CPF");
		
		String camelCase = "CPF";
		List<String> listaRetornada = ccImpl.toList(camelCase);
		
		assertNotNull(listaRetornada);
		assertEquals(listaEsperada.size(),listaRetornada.size());
		assertEquals(listaEsperada.get(0), listaRetornada.get(0));
	}
	
	@Test
	public void toList_PALAVRAS_MINUSCULAS_MAIUSCULAS() {
		
		final List<String> listaEsperada = Arrays.asList("numero","CPF");
		
		String camelCase = "numeroCPF";
		List<String> listaRetornada = ccImpl.toList(camelCase);
		
		assertNotNull(listaRetornada);
		assertEquals(listaEsperada.size(),listaRetornada.size());
		assertEquals(listaEsperada.get(0), listaRetornada.get(0));
		assertEquals(listaEsperada.get(1), listaRetornada.get(1));
	}
	
	@Test
	public void toList_PALAVRAS_MINUSCULAS_MAIUSCULAS_2() {
		
		final List<String> listaEsperada = Arrays.asList("numero","CPF","contribuinte");
		
		String camelCase = "numeroCPFContribuinte";
		List<String> listaRetornada = ccImpl.toList(camelCase);
		
		assertNotNull(listaRetornada);
		assertEquals(listaEsperada.size(),listaRetornada.size());
		assertEquals(listaEsperada.get(0), listaRetornada.get(0));
		assertEquals(listaEsperada.get(1), listaRetornada.get(1));
		assertEquals(listaEsperada.get(2), listaRetornada.get(2));
	}

	@Test
	public void toList_PALAVRAS_MINUSCULAS_MAIUSCULAS_3() {
		
		final List<String> listaEsperada = Arrays.asList("numero","CPF","contribuinte","CNPJ","empresa");
		
		String camelCase = "numeroCPFContribuinteCNPJEmpresa";
		List<String> listaRetornada = ccImpl.toList(camelCase);
		
		assertNotNull(listaRetornada);
		assertEquals(listaEsperada.size(),listaRetornada.size());
		assertEquals(listaEsperada.get(0), listaRetornada.get(0));
		assertEquals(listaEsperada.get(1), listaRetornada.get(1));
		assertEquals(listaEsperada.get(2), listaRetornada.get(2));
		assertEquals(listaEsperada.get(3), listaRetornada.get(3));
		assertEquals(listaEsperada.get(4), listaRetornada.get(4));
	}
	
	@Test
	public void toList_PALAVRAS_NUMEROS() {
		
		final List<String> listaEsperada = Arrays.asList("recupera","10","primeiros");
		
		String camelCase = "recupera10Primeiros";
		List<String> listaRetornada = ccImpl.toList(camelCase);
		
		assertNotNull(listaRetornada);
		assertEquals(listaEsperada.size(),listaRetornada.size());
		assertEquals(listaEsperada.get(0), listaRetornada.get(0));
		assertEquals(listaEsperada.get(1), listaRetornada.get(1));
		assertEquals(listaEsperada.get(2), listaRetornada.get(2));
	}
	
	@Test(expected=RegraNegocioException.class)
	public void toList_VALIDACAO_INICIANDO_NUMEROS_1() {
		String mensagemEsperada = "Não deve começar com números";
		String camelCase = "10Primeiros";
		
		try {
			ccImpl.toList(camelCase);
			fail("Uma exception deveria ter sido lançada!");
		} catch (RegraNegocioException e) {
			assertEquals(mensagemEsperada, e.getMessage());
			throw e;
		}
	}

	@Test(expected=RegraNegocioException.class)
	public void toList_VALIDACAO_INICIANDO_NUMEROS_2() {
		String mensagemEsperada = "Não deve começar com números";
		String camelCase = "5Primeiros";
		
		try {
			ccImpl.toList(camelCase);
			fail("Uma exception deveria ter sido lançada!");
		} catch (RegraNegocioException e) {
			assertEquals(mensagemEsperada, e.getMessage());
			throw e;
		}
	}
	
	@Test(expected=RegraNegocioException.class)
	public void toList_VALIDACAO_INICIANDO_NUMEROS_3() {
		String mensagemEsperada = "Não deve começar com números";
		String camelCase = "9Primeiros";
		
		try {
			ccImpl.toList(camelCase);
			fail("Uma exception deveria ter sido lançada!");
		} catch (RegraNegocioException e) {
			assertEquals(mensagemEsperada, e.getMessage());
			throw e;
		}
	}
	
	@Test(expected=RegraNegocioException.class)
	public void toList_VALIDACAO_CONTENDO_CARACTER_INVALIDO() {
		String mensagemEsperada = "Caracteres especiais não são permitidos, somente letras e números";
		String camelCase = "nome#Composto";
		
		try {
			ccImpl.toList(camelCase);
			fail("Uma exception deveria ter sido lançada!");
		} catch (RegraNegocioException e) {
			assertEquals(mensagemEsperada, e.getMessage());
			throw e;
		}
	}
	
	
	
}
